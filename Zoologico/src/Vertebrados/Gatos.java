package Vertebrados;

public class Gatos {
		//atributos
			private String pelagem;
			private char porte;
			private String ra�a;
			private char genero;
			private float peso;
			private String cor;
			private int idade;
			
		//m�todos de acesso
			
			public String getPelagem() {
				return pelagem;
			}

			public void setPelagem(String pelagem) {
				this.pelagem = pelagem;
			}

			public char getPorte() {
				return porte;
			}

			public float getPeso() {
				return peso;
			}

			public void setPeso(float p) {
				peso = p;
			}

			public int getIdade() {
				return idade;
			}

			public void setIdade(int idade) {
				this.idade = idade;
			}

			//m�todo construtor
			public Gatos(String ra�a, String cor1, char gen){
				this.ra�a=ra�a;
				cor=cor1;
				this.genero=gen;
				
			}
			//m�todos de classes
			public void miar(){
				System.out.println("Oh gato chato..miau.miau!!");
			}
			
			public void morder(){
				System.out.println("Ai!! Me mordeu..");
			}
			
			public boolean arranhar(boolean obd){
				if (obd) {
					System.out.println("Me arranhou!!!");
				}
				else
				{
					System.out.println("Como � carinhoso");
				}
				
				return obd;
			}
			public static void main(String[] args) {
				// TODO Auto-generated method stub
				
				Gatos g1=new Gatos("Viralata","branco/preto",'F');
				g1.setIdade(8);
				Gatos g2=new Gatos("Persa","bege",'M');
				g2.miar();
				Gatos g3=new Gatos("Gato de Bengala","Malhado",'F');
				g3.arranhar(true);
				Gatos g4=new Gatos("Siames","Branco",'M');
				g4.arranhar(false);

			}
		}
